import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the BmiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class BmiProvider {

  constructor() {
   
  }

  calculateBMI(Height:number,Weight:number)
  {
    return Weight / Height / Height ;
  }

  ClassifyBMI(BMI:number)
  {
    if(BMI<18.5)
      return 'Underweight';
    else if (BMI>18.5 && BMI<24.9)
      return 'Normal weight';
    else if (BMI>25 && BMI<29.9)
      return 'over weight';
    else if (BMI>30 && BMI<34.9)
      return 'class 1 obesity';
    else if (BMI>35 && BMI<39.9)
      return 'class 2 obesity';
    else if (BMI>40)
      return 'class 3 obesity';
  }

}
