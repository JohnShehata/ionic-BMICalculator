import { BmiProvider } from './../../providers/bmi/bmi';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the BmiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bmi',
  templateUrl: 'bmi.html',
})
export class BmiPage {
  Height:number;
  Weight:number;
  BMIResult:number=0
  BMIClassification:string
  

  constructor(public navCtrl: NavController, public navParams: NavParams,private BMIProvider:BmiProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BmiPage');
  }

  CalculateBMI(){
   this.BMIResult=this.BMIProvider.calculateBMI(this.Height,this.Weight);
   this.BMIClassification=this.BMIProvider.ClassifyBMI(this.BMIResult);
  }
}
